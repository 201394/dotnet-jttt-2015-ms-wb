﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HtmlAgilityPack;
using System.Net;
using System.Windows.Forms;

namespace JTTT2
{
    public class html
    {
        private string url;
        private string tekst;

        public html(string url, string tekst)
        {
            this.url = url;
            this.tekst = tekst;
        }

        public string PobierzHtml()
        {
            using (WebClient wc = new WebClient())
            {
                string zawartosc = wc.DownloadString(url);
                return zawartosc;
            }
        }

        public string Przeszukiwanie()
        {
            // Tworzymy obiekt klasy HtmlDocument zdefiniowanej w namespace HtmlAgilityPack

            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

            string pageHtml = PobierzHtml();

            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");

            string tytul;
            string strona = "";
            string nazwa_obrazka = tekst;

            foreach (var node in nodes)
            {
                try
                {
                    tytul = node.GetAttributeValue("alt", "").ToUpper();

                    if (tytul.Contains(tekst.ToUpper()))
                    {
                        strona = node.GetAttributeValue("src", ""); //zwraca adres url do zdjecia
                        using (WebClient wc = new WebClient())
                        {
                            wc.DownloadFile(strona, nazwa_obrazka);
                        }
                    }
                }
                catch(ArgumentNullException a)
                {
                    MessageBox.Show("Failed to GetAtribute. Reason: " + a.Message);
                    //throw;
                }

            }
            return nazwa_obrazka;
        }
    }
}
