﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT2
{
    public partial class WyswietlanieWyniku : Form
    {
        string nazwa_obrazka { get; set; }
        string tresc { get; set; }

        public WyswietlanieWyniku(string nazwa_obrazka, string tresc)
        {
            InitializeComponent();
            this.nazwa_obrazka = nazwa_obrazka; 
            this.tresc = tresc;
            this.Load += WyswietlanieWyniku_Load;
        }

        private void  WyswietlanieWyniku_Load(object sender, EventArgs e)
        {
            try
            {
                pictureBox1.Image = Image.FromFile(nazwa_obrazka);
                pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            }
            catch(FileNotFoundException a)
            {
                MessageBox.Show("Failed to FileNotFoundException. Reason: " + a.Message);
                //throw;
            }
            richTextBox1.AppendText(tresc);
        }
    }
}
