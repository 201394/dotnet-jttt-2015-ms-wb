﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace JTTT2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        ListaZadan lista = new ListaZadan();
        Obsluga_Bazy Obsluga_Bazy = new Obsluga_Bazy(); 

        private void Form1_Load(object sender, EventArgs e)
        {
            Obsluga_Bazy.WczytajBaze(lista);  //Wczytywanie zawartosci bazy do list boxa przy starcie programu
            listBox1.DataSource = lista;                   
        }

        private void button1_Click(object sender, EventArgs e)      //WYPELNIANIE LISTBOXA I BAZY DANYCH
        {
            if(tabControl1.SelectedTab==tabPage1)
            {
                if(tabControl2.SelectedTab==tabPage3)
                {
                    var zad = new Zadanie(URL.Text, Tekst.Text, Email.Text);
                    lista.Dodaj(zad);
                    Obsluga_Bazy.DodajPozycje(zad); 
                }
                else
                {
                    var zad = new Zadanie(URL.Text, Tekst.Text);
                    lista.Dodaj(zad);
                    Obsluga_Bazy.DodajPozycje(zad); 
                }
            }
            else
            {
                if (tabControl2.SelectedTab == tabPage3)
                {
                    var zad = new Zadanie(Miasto.Text, (int)Temperatura.Value, Email.Text);
                    lista.Dodaj(zad);
                    Obsluga_Bazy.DodajPozycje(zad); 
                }
                else
                {
                    var zad = new Zadanie(Miasto.Text, (int)Temperatura.Value);
                    lista.Dodaj(zad);
                    Obsluga_Bazy.DodajPozycje(zad); 
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)      //WYKONANIE ZADAN Z LISTBOXA
        {
            lista.Wykonaj();

        }

        private void button3_Click(object sender, EventArgs e)
        {
 
            Obsluga_Bazy.Wyczysc();     //CZYSZCZE BAZE DANYCH         

            lista.Wyczysc();    //CZYSZCZE LISTE 
        }

        //Serializacja
        private void button5_Click(object sender, EventArgs e)
        {

            FileStream fs = new FileStream("DataFile.dat", FileMode.Create);


            BinaryFormatter formatter = new BinaryFormatter();
            try
            {
                formatter.Serialize(fs, lista);
            }
            catch (SerializationException a)
            {
                MessageBox.Show("Failed to serialize. Reason: " + a.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }

        //Deserializajca
        private void button4_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("DataFile.dat", FileMode.Open);
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                lista.Wyczysc();
                foreach (Zadanie zad in (ListaZadan)formatter.Deserialize(fs))
                {
                    lista.Dodaj(zad);
                }
            }
            catch (SerializationException a)
            {
                MessageBox.Show("Failed to deserialize. Reason: " + a.Message);
                throw;
            }
            finally
            {
                fs.Close();
            }
        }


    }
}
