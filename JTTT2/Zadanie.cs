﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JTTT2
{
    [Serializable()]
    class Zadanie
    {
        public string URL { get; set; }
        public string Tekst { get; set; }
        public string Miasto { get; set; }
        public int? Temperatura { get; set; }
        public string Email { get; set; }

        public Zadanie( string URL, string Tekst, string Email)
        {
            this.URL = URL;
            this.Tekst = Tekst;
            this.Email = Email;
            this.Miasto = null;
            this.Temperatura = null;
        }

        public Zadanie(string Miasto, int Temperatura, string Email)
        {
            this.URL = null;
            this.Tekst = null;
            this.Email = Email;
            this.Miasto = Miasto;
            this.Temperatura = Temperatura;
        }

        public Zadanie(string URL, string Tekst)
        {
            this.URL = URL;
            this.Tekst = Tekst;
            this.Email = null;
            this.Miasto = null;
            this.Temperatura = null;
        }

        public Zadanie(string Miasto, int Temperatura)
        {
            this.URL = null;
            this.Tekst = null;
            this.Email = null;
            this.Miasto = Miasto;
            this.Temperatura = Temperatura;
        }

        public override string ToString()
        {
            if (URL != null && Tekst != null && Email != null)
                return string.Format("Na {0} znajdz {1} i wyslij na {2}", URL, Tekst, Email);
            if (URL != null && Tekst != null && Email == null)
                return string.Format("Na {0} znajdz {1} i wyswietl", URL, Tekst);
            if (Miasto != null && Email != null)
                return string.Format("Jezeli w {0} jest wieksza temperatura niz {1} to wyslij na {2}", Miasto, Temperatura, Email);
            if (Miasto != null && Email == null)
                return string.Format("Jezeli w {0} jest wieksza temperatura niz {1} to wyswietl", Miasto, Temperatura);
            else
                return "Brak specyfikacji zadania";
        }

        public void WykonanieZadania()
        {
            if (URL != null)
            {
                var hs = new html(URL,Tekst);
                string nazwa_obrazka = hs.Przeszukiwanie();
                if (Email != null)
                {
                    var wy = new Wysylanie(nazwa_obrazka, Email);
                    wy.WyslijWiadomosc();
                   
                }
                else
                {
                    var wys = new WyswietlanieWyniku(nazwa_obrazka,Tekst);
                    wys.Show();
                   
                }
            }
            else
            {
                var po = new Pogoda();
                po.PobierzMiasto(Miasto,(int)Temperatura);
                po.PobierzJson();
                string tresc = po.StworzKomunikat();
                string nazwa_obrazka = po.PobierzIkone();
                if (Email != null)
                {
                    var wy = new Wysylanie(nazwa_obrazka, Email, tresc);
                    wy.WyslijWiadomosc();
                   
                }
                else
                {
                    var wys = new WyswietlanieWyniku(nazwa_obrazka, tresc);
                    wys.Show();
                    
                }
            }
        }

    }
}
