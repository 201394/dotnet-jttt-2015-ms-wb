﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT2
{
   public class ZadanieBaza
    {
        public int Id { get; set; }
        public string URL { get; set; }
        public string Tekst { get; set; }
        public string Email { get; set; }
        public string Miasto { get; set; }
        public int? Temperatura { get; set; }

        public override string ToString()
        {
            return string.Format("Id={0}, URL={1},Tekst={2},Email={3},Miasto={4},Temperatura={5}", Id,URL,Tekst,Email,Miasto,Temperatura);
        }
   
   }
}
