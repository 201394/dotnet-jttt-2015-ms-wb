﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JTTT2
{
    class Obsluga_Bazy
    {


        public void WczytajBaze(ListaZadan lista)
        {
            using (var ctx = new BazaDanychDbContext())
            {
                foreach (var t in ctx.ZadanieBaza)
                {
                    if (t.URL != null && t.Tekst != null && t.Email != null)
                    {
                        var zad = new Zadanie(t.URL, t.Tekst, t.Email);
                        lista.Dodaj(zad);
                    }
                    if (t.URL != null && t.Tekst != null && t.Email == null)
                    {
                        var zad = new Zadanie(t.URL, t.Tekst);
                        lista.Dodaj(zad);
                    }
                    if (t.Miasto != null && t.Email != null)
                    {
                        var zad = new Zadanie(t.Miasto, (int)t.Temperatura.Value, t.Email);
                        lista.Dodaj(zad);
                    }
                    if (t.Miasto != null && t.Email == null)
                    {
                        var zad = new Zadanie(t.Miasto, (int)t.Temperatura.Value);
                        lista.Dodaj(zad);
                    }
                }
            }
        }


        public void DodajPozycje(Zadanie zad)
        {
            using (var ctx = new BazaDanychDbContext())
            {
                // Tworzymy baze w pamięci
                ZadanieBaza baza = new ZadanieBaza() {URL=zad.URL, Tekst=zad.Tekst, Email=zad.Email, Miasto = zad.Miasto, Temperatura=zad.Temperatura };

                // Baze dodajemy do kolekcji reprezentującej dane w bazie danych
                ctx.ZadanieBaza.Add(baza);
                // Zapisujemy zmienne przechowywane w kontekście
                ctx.SaveChanges();
            }
        }

        public void Wyczysc()       //CZYSZCZENIE BAZY 
        {
            using (var ctx = new BazaDanychDbContext())
            {
                foreach(var i in ctx.ZadanieBaza)
                ctx.ZadanieBaza.Remove(i);
                ctx.SaveChanges();
            }


        
        }
    }
}
