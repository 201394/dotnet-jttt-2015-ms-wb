﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace JTTT2
{
    [Serializable()]
    class ListaZadan : BindingList<Zadanie>
    {
        public ListaZadan()
        {
            new BindingList<Zadanie>();
        }
        public ListaZadan(Zadanie zad)
        {
            Add(zad);
        }
        public void Dodaj(Zadanie zad)
        {
            Add(zad);
        }
        public void Wyczysc()
        {
            Clear();
        }
        public int Rozmiar()
        {
            return base.Count;
        }

        public void Wykonaj()
        {
            for (int i=0; i<Rozmiar(); i++)
            {
                this[i].WykonanieZadania();
            }
        }
    }
}