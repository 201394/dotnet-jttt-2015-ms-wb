﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace JTTT2
{
    class Pogoda
    {
        string json;
        string miasto;
        int temperatura;
        string komunikat;
        string ikona;

        public void PobierzMiasto(string Miasto, int Temperatura)
        {
            miasto = Miasto;
            temperatura = Temperatura;

        }

        public void PobierzJson()
        {
         using (WebClient wc = new WebClient())
            {
                json = wc.DownloadString("http://api.openweathermap.org/data/2.5/weather?q="+miasto);
            }
        }

        public string StworzKomunikat()
        {
            var pogoda = JsonConvert.DeserializeObject<DanePogody.RootObject>(json);

            if (pogoda.main.temp - 273.15 >= temperatura)
            {
                komunikat = pogoda.ToString();
                ikona = "http://openweathermap.org/img/w/" + pogoda.weather[0].icon + ".png";
            }
            else
                komunikat = String.Format("Aktualnie we {0} jets chłodniej niż się spodziewałeś : {1}", pogoda.name, pogoda.main.temp - 273.15);

            
            return komunikat;
        }

        public string  PobierzIkone ()
        {
            string nazwa_obrazka = miasto;
            if (ikona != null)
            {
                using (WebClient wc = new WebClient())
                {
                    wc.DownloadFile(ikona, nazwa_obrazka);
                }
            }

            return nazwa_obrazka;
        }
    }
}
