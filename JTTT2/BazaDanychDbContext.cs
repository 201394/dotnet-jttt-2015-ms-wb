﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace JTTT2
{
    class BazaDanychDbContext : DbContext
    {
          public BazaDanychDbContext()
            : base("Dane Do JTTT")
        {
            // Użyj klasy StudiaDbInitializer do zainicjalizowania bazy danych.
            Database.SetInitializer<BazaDanychDbContext>(new BazaDanychDbInitializer());
        }
          public DbSet<ZadanieBaza> ZadanieBaza { get; set; }
    }
}
